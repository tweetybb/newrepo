/*
Created By       : Dharshini Sivam
Revision History : 'LeadManager_AC_Test' Test Class Created On 01/25/2019
Class Nature     : Test class for webservice Lead 
Summary          : Testing whether response for callout for postive and negative scenario.
*/
@isTest
private class webservice_Lead_Ac_Test {
    @isTest static void testNotConvertedLead()
    {
        //inserting new lead
        Lead testLd = new Lead(LastName ='Lead_Test', Company='Com_Test', Status='Open - Not Contacted' );
        insert testLd;
        
        // Set up a test request
        RestRequest request1 = new RestRequest();
        request1.requestUri = 'https://ap4.salesforce.com/services/apexrest/Leads/'+ testLd.Id;
        request1.httpMethod = 'GET';
        RestContext.request = request1;
        
        // Call the method to test
        String notConvertedLead = webservice_Lead_Ac.getLeadStatusById();
        System.assertEquals('Lead is not converted', notConvertedLead, 'Lead is converted');//checking for not converted lead
    }
    //method to check converted lead
    @isTest static void testConvertedLead()
    {
        
        Lead testLead1 = new Lead(LastName ='Lead_Test', Company='Com_Test', Status='Open - Not Contacted' );
        insert testLead1;
        
        //converting lead
        Database.LeadConvert lead = new Database.LeadConvert();
        lead.setLeadId(testLead1.Id);
        lead.setConvertedStatus('Closed - Converted');
        lead.setDoNotCreateOpportunity(True);
        Database.convertLead(lead);
        
        RestRequest request2 = new RestRequest();
        request2.requestUri = 'https://ap4.salesforce.com/services/apexrest/Leads/'+ testLead1.Id;
        request2.httpMethod = 'GET';
        RestContext.request = request2;
        
        // Call the method to test
        String ConvertTest = webservice_Lead_Ac.getLeadStatusById();
        
        Lead output =  [SELECT Name, IsConverted, Owner.Name, status  FROM Lead WHERE Id = :testLead1.Id];
        System.assertEquals(output.Owner.Name, ConvertTest, 'Lead is not converted');//checking for converted lead
    }
}
----------------------------------------------------------------------------
/*
Created By       : Dharshini Sivam
Revision History : 'webservice_Lead_Ac' Created On 01/25/2019
Test Class       : 'webservice_Lead_Ac_Test' Test Class Created On 01/25/2019
Class Nature     : Webservices
Summary          : Webservie for responding to callout about the lead status

*/
@RestResource(urlMapping='/Leads/*')
global with sharing class webservice_Lead_Ac {
    //get method annotation
    @HttpGet
    global static string getLeadStatusById() {
        //getting request from the callout
        RestRequest getRequest = RestContext.request;
        // grab the leadId from the end of the URL
        String leadId = getRequest.requestURI.substring(getRequest.requestURI.lastIndexOf('/')+1);
        
        //querying the lead record with requested lead Id
        Lead respResult = [SELECT Name, IsConverted, Owner.Name, status FROM Lead WHERE Id = :leadId];
        
        if(respResult.IsConverted)
        {
            return respResult.Owner.Name;//result sent if lead is converted
        }
        else
        {
            return 'Lead is not converted';//result sent if the lead is not converted
        }
    }
}


