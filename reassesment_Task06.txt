/* 
Created By                : Dharshini Sivam
Revision History      : 'ContactTrigger_AT' created on 01/28/2019
Test Class                  : 'UpdateAccAddress_AC_Test'created on 01/28/2019
Class Nature             :  Trigger 
Summary                  :  Trigger on contact to update account address
*/
trigger ContactTrigger_AT on Contact (after insert, after update) {
    if(Trigger.isInsert|| Trigger.isUpdate)
    {
        UpdateAccAddress_AC.updateaddress(Trigger.New);
        UpdateAccAddress_AC.userinfo(Trigger.NewMap.KeySet());
    }
    
}
---------------------------------------------------------------------------------------------------------------
/* 
Created By                    :  Dharshini Sivam
Revision History           : 'UpdateAccAddress_AC' created on 01/28/2019
Test Class                      :  'UpdateAccAddress_AC_Test'created on 01/28/2019
Class Nature                 :  Handler class 
Summary                      :  Handlerclass for updating the address of account and user address when the address is populated in contact
*/
public class UpdateAccAddress_AC {
    public static void updateaddress(List<Contact> conList)
    {
        Map<Id,Contact> IdMap = new Map<Id,Contact>();
        
        for(contact con : conList)
        {
            IdMap.put(con.AccountId,con);
        }
        List<Account> accToUpdList = [SELECT Id, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM 
                                      Account WHERE ID IN  :IdMap.KeySet()];
        //Storing the address of contact in particular account
        
        for(Account acc: accToUpdList)
        {
            acc.BillingStreet      =  IdMap.get(acc.Id).MailingStreet;
            acc.BillingCity        =  IdMap.get(acc.Id).MailingCity;
            acc.BillingState       =  IdMap.get(acc.Id).MailingState;
            acc.BillingCountry     =  IdMap.get(acc.Id).MailingCountry;
            acc.BillingPostalCode  =  IdMap.get(acc.Id).MailingPostalCode;  
        }
        
        if(accToUpdList.size()>0)
            update accToUpdList;  //Update the account's billing address
    }
    @future
    public static void userinfo(Set<Id> conId)
    {
        Map<Id,Contact> IdMap = new Map<Id,Contact>();
        List<Contact> contactList = [SELECT Id, MailingStreet, MailingState, MailingCity, MailingCountry, MailingPostalCode,
                                     Volunteer_User__c FROM contact WHERE Id IN :conId];
        
        
        for(contact cont : contactList)
        {
            IdMap.put(cont.Volunteer_User__c,cont);
        }
        
        List<User> userUpdationList = [SELECT Id,Street,City,State,Postalcode,Country FROM User WHERE ID In :IdMap.KeySet()]; 
        //Storing the values of address in selected user 
        for(User usr : userUpdationList)
        {
            usr.State      =  IdMap.get(usr.Id).MailingState;
            usr.Street     =  IdMap.get(usr.Id).MailingStreet;
            usr.City       =  IdMap.get(usr.Id).MailingCity;
            usr.Country    =  IdMap.get(usr.Id).MailingCountry;
            usr.PostalCode =  IdMap.get(usr.Id).MailingPostalCode;
            
        }
        System.debug(userUpdationList);
        if(userUpdationList.size()>0)
            update userUpdationList; //Update the user's address
    }
}
---------------------------------------------------------------------------------------------------------------
/* 
Created By                : Dharshini Sivam
Revision History      : 'UpdateAccAddress_AC_Test' created on 01/28/2019
Class Nature             :  Testclass
Summary                  :  Test class for creation of accounts and contacts and to update the user mailing address and account 
billing address if the Contact address is updated
*/
@isTest
public class UpdateAccAddress_AC_Test {
    //normal method for build accounts and contacts
    public static List<Account> buildAccounts(integer accCount)
    {
        List<Account> accountsList = new List<Account>();
        
        for(integer i=1;i<=accCount;i++)
        {
            Account accRec = new Account(Name='Account'+ i);
            accountsList.add(accRec);
        }
        return accountsList;
    }
    
    public static list<Contact>createcontacts(Id AccountId,Integer howMany)  
    {
        List<Contact> conList = new List<Contact>();
        List<User> userList = [SELECT Id FROM User WHERE Profile.Name='Standard Platform User' LIMIT 1];
        for(integer i=1;i<=howMany;i++)
        {
            Contact con = new Contact(AccountId = AccountId, LastName = 'Abc'+i, MailingStreet='woraiyur', MailingCity='Trichy'+i,  
                                      MailingState='TamilNadu', MailingCountry='India', MailingPostalCode = '620003',
                                      Volunteer_User__c = userList[0].Id);
            conList.add(con);
        }
        return conList;
        
    }
    //Test method to create account and contacts to update that list
    @isTest static void conRecord(){
        List<Account> accList = buildAccounts(10); 
        insert accList;
        List<Contact> contactList = new List<Contact>();
        for(Account var :accList)
        {
            List<Contact> conCreate = createcontacts(var.Id,20);
            contactList.addAll(conCreate);
        }
        Test.startTest();
        insert contactList;
        Test.stopTest();
        List<Account> acctList = [SELECT BillingStreet FROM Account WHERE Id IN :accList];
        System.assertEquals('woraiyur',acctList[0].BillingStreet);
    }
    @isTest static void conRecord2(){
        List<Account> accList = buildAccounts(10); 
        insert accList;
        List<Contact> contactsList = new List<Contact>();
        for(Account var :accList)
        {
            List<Contact> conCreate = createcontacts(var.Id,20);
            contactsList.addAll(conCreate);
        }
        Test.startTest();
        insert contactsList;
        
        List<Contact> conUpdateList = [SELECT Id, MailingStreet FROM Contact WHERE Id IN :contactsList];
        System.debug(conUpdateList);
        for(Contact conUp : conUpdateList)
        {
            conUp.MailingStreet = 'KKNagar';
        }
        update conUpdateList;
        Test.stopTest();
        
        List<Contact> conUpdCheckList = [SELECT Id, AccountId FROM Contact WHERE Id IN :conUpdateList];
        
        List<Account> updateCheckList = [SELECT BillingStreet FROM Account WHERE Id = :conUpdCheckList[0].AccountId];
        System.assertequals('KKNagar', updateCheckList[0].BillingStreet);
        
        System.debug(updateCheckList); 
        
        List<Contact> conListUpdate = [SELECT Id, AccountId, Volunteer_User__c FROM Contact WHERE Id IN :conUpdCheckList];
        
        List<User> volUserRecList = [SELECT Id, Address, Street FROM User WHERE Id = :conListUpdate[0].Volunteer_User__c];
        System.assertEquals('KKNagar', volUserRecList[0].Street, 'Street shold be updated to KKNagar ');
        
        List<Account> updatedListAcc = [SELECT Id, BillingStreet FROM Account WHERE Id = :conupdCheckList[0].AccountId];
        System.assertEquals('KKNagar', updatedListAcc[0].BillingStreet, 'Billing street shold be updated to KKNagar');
    }
    
}